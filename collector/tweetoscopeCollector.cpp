/* To compile: 
g++ -o tweetcollector tweetoscopeCollector.cpp -O3 $(pkg-config --cflags --libs gaml) -lpthread -lcppkafka
*/

#include "tweetoscopeCollectorParams.hpp"
#include "tweetoscopeCollector.hpp"
#include "JSONReader.hpp"
#include <cppkafka/cppkafka.h>
#include <iostream>
#include <typeinfo>


/*********************************
****** Function Definitions ******
*********************************/

/**********************************
**** Write Msg to Kafka Topics ****
**********************************/

// Send partial cascade to Kafka topic Kafka Cascade_Series:	
void send_to_cascade_series(const std::size_t& window_first ,tweetoscope::processing::cascade_ref sp, 
cppkafka::MessageBuilder& builder_series, cppkafka::Producer& producer){
	
	builder_series.key(); //no spefic key is added.
	
	std::ostringstream ostr;
	ostr 	<< "{"
			<< "\"type\": \"serie\""
			<< ", \"cid\": " 		<< sp->cascade_id_
			<< ", \"msg\": " 		<< sp->cascade_name_
			<< ", \"T_obs\": " 		<< window_first
			<< ", \"tweets\": [ ";
	for(auto& [ti,mi] : sp->time_serie_) ostr << "(" << ti << ", " << mi << "), ";
	ostr 	<< "] }" ;

	auto msg_out = ostr.str();
	builder_series.payload(msg_out);
	producer.produce(builder_series);
	sleep(1); //necessary for slow connections between nodes
	producer.flush();
}

// Send debug Message to Kafka topic logs about partial cascade:
void send_partial_to_logs(const std::size_t& window_first ,tweetoscope::processing::cascade_ref sp, 
cppkafka::MessageBuilder& builder_logs, cppkafka::Producer& producer, const tweetoscope::params::collector& params){

	std::ostringstream ostr;
	ostr 	<< "Cascade " 		<< sp->cascade_id_ 
			<< ": time window " << window_first 
			<< " sent to " 		<< params.topic.out_series 
			<< ".";

	std::cout << ostr.str() << std::endl;

	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_c = std::chrono::system_clock::to_time_t(now);									

	std::ostringstream ostr2;
	ostr2	<< 	"{\"t\": "	<< now_c
			<< ", \"source\": \"collector-node\", \"level\": \"DEBUG\", \"message\": \""
			<<	ostr.str()
			<<	"\"}";
	
	builder_logs.key(params.topic.out_logs);
	auto msg_out = ostr2.str();
	builder_logs.payload(msg_out);
	producer.produce(builder_logs);
	sleep(1); //necessary for slow connections between nodes
	producer.flush();
}

// Sends cascade to Kafka topic Cascade_Properties:
void send_to_cascade_properties(const std::size_t& observation_time, tweetoscope::processing::cascade_ref sp_top, 
cppkafka::MessageBuilder& builder_properties, cppkafka::Producer& producer){

	std::string	key_str = std::to_string(static_cast<int>(observation_time));
	builder_properties.key(key_str); //corresponds to time window
	
	std::ostringstream ostr;
	ostr 	<< "{"
			<< "\"type\": \"size\""
			<< ", \"cid\": " 		<< sp_top->cascade_id_
			<< ", \"n_tot\": " 		<< sp_top->time_serie_.size()
			<< ", \"t_end\": " 		<< sp_top->time_serie_.back().first
			<< "}";

	auto msg_out = ostr.str();
	builder_properties.payload(msg_out);
	producer.produce(builder_properties);
	sleep(1); //necessary for slow connections between nodes
	producer.flush();
}

//Send Debug Message to topic logs about cascade terminaison:
void send_terminaison_to_logs(const std::size_t& observation_time ,tweetoscope::processing::cascade_ref sp_top, 
cppkafka::MessageBuilder& builder_logs, cppkafka::Producer& producer, const tweetoscope::params::collector& params){


	std::string	key_str = std::to_string(static_cast<int>(observation_time));
	std::ostringstream ostr;
	ostr 	<< "Cascade " 					<< sp_top->cascade_id_ 
			<< ": full cascade sent to " 	<< params.topic.out_properties
			<< ".";

	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_c = std::chrono::system_clock::to_time_t(now);									

	std::ostringstream ostr2;
	ostr2	<< 	"{\"t\": "	<< now_c
			<< ", \"source\": \"collector-node\", \"level\": \"DEBUG\", \"message\": \""
			<<	ostr.str()
			<<	"\"}";
	
	builder_logs.key(params.topic.out_logs);
	auto msg_out = ostr2.str();
	builder_logs.payload(msg_out);
	producer.produce(builder_logs);
	sleep(1); //necessary for slow connections between nodes
	producer.flush();
}


/*********************************
**** Partial Windows Function ****
*********************************/

// Updates partial cascades and sends to Kafka topics Cascade_Series & Logs:
void test_partial_cascade(tweetoscope::processing::Processor& processor, cppkafka::MessageBuilder& builder_series, cppkafka::MessageBuilder& builder_logs, 
cppkafka::Producer& producer, const std::pair<std::size_t, double>& pair_t_m, const tweetoscope::params::collector& params){

	for ( auto& window : processor.windows_cascades_queue_ ){
		
		std::size_t previous_size = 0;
		while (window.second.size() != 0 && previous_size != window.second.size()){
			previous_size = window.second.size();

			auto wp = window.second.front(); //on regarde la cascade la plus ancienne de la window
			if(auto sp = wp.lock(); sp) { //cascade not terminated
				if (window.first < pair_t_m.first-sp->time_serie_.front().first ){ // on regarde la durée de la cascade

					window.second.pop(); //remove the cascade from window

					if (params.cascade.min_cascade_size <= sp->time_serie_.size()){ 
						// on regarde si la taille est suffisante pour être envoyée
						
						// Envoi de la cascade dans le topic Kafka Cascade_Series:	
						send_to_cascade_series(window.first, sp, builder_series, producer);

						// Send debug Message to Kafka topic logs:
						send_partial_to_logs(window.first, sp, builder_logs, producer, params);										
					}
				}
			}
			
			else{//No shared pointer = cascade was already terminated
				window.second.pop();
				std::cout << std::endl << "A terminated cascade was removed from time window: \""
				<< window.first << "\".";
			}	
		}
	}
}


/*******************************
***** Terminaison Function *****
*******************************/

// Updates terminated Cascades and sends to Kafka topics Cascade_Properties & Logs:
void test_cascade_terminaison_and_update(const std::string& processor_name, tweetoscope::processing::Processor& processor, cppkafka::MessageBuilder& builder_properties, 
cppkafka::MessageBuilder& builder_logs, cppkafka::Producer& producer, const std::pair<std::size_t, double>& pair_t_m, const tweetoscope::params::collector& params){

	std::size_t previous_size = 0;
	while (processor.cascades_queue_.size() != 0 && previous_size != processor.cascades_queue_.size()){
		previous_size =processor.cascades_queue_.size();
		
		std::shared_ptr<tweetoscope::processing::Cascade> sp_top = processor.cascades_queue_.top();
		if(params.times.terminated <= pair_t_m.first-sp_top->time_serie_.back().first){ //no retweet received in a while
			
			// Envoi en x exemplaires la cascade dans le topic Cascade_Properties à condition que sa longueur soit suffisante:
			if (params.cascade.min_cascade_size <= sp_top->time_serie_.size()){ 							
				for(auto observation_time : params.times.observation){
					
					// Send cascade to topic Cascade_Properties
					send_to_cascade_properties(observation_time, sp_top, builder_properties, producer);

					// Send Debug Message to topic logs about cascade terminaison:
					send_terminaison_to_logs(observation_time, sp_top, builder_logs, producer, params);
				}
			}

			// Remove the terminated cascade:
			processor.cascades_queue_.pop();
			// Remove from Location_ ?

			std::cout << std::endl << "Cascade " << sp_top->cascade_id_ 
			<< ": terminated." << std::endl;		
		}
	}
}


/**********************************
*** Retweet Processing Function ***
**********************************/

// Processes retweet information:
void process_retweet(const std::size_t& key, const tweetoscope::tweet& twt, tweetoscope::processing::Processor& processor, 
const std::pair<std::size_t, double>& pair_t_m){

	std::ostringstream ostr;
	ostr	<< "Retweet of source: \"" 			<< twt.source					
			<< "\" and key: \"" 				<< key 
			<< "\" is handled by processor: \"" << processor.source_id_ 
			<< "\" ";	

	auto it_c =	processor.locations_.find(key);
	//identifie la cascade responsable du retweet

	if ( it_c != processor.locations_.end() ){

		auto wp = it_c->second; // récupère le weak pointer sur la cascade
		if(auto sp = wp.lock(); sp) {

			//ajoute pair_t_m à la série temporelle
			sp->time_serie_.push_back(pair_t_m);
			
			//sets cascade_level to the retweet's time and update de priority queue
			*sp=pair_t_m.first;
			processor.cascades_queue_.update(sp->location_);								

			ostr 	<< "and cascade: \"" << sp->cascade_id_ 
					<< "\".";

			// std::cout << ostr.str() <<std::endl; //for debug
		}
		else{
			//std::cout << std::endl << "A terminated cascade was removed from Locations_." << std::endl;
		}
	}	
}


/*************************
********** Main **********
*************************/

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }

  tweetoscope::params::collector params(argv[1]);
  
  std::cout << std::endl
        << "Parameters : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;

	cppkafka::Configuration config {
	  {"metadata.broker.list", params.kafka.brokers},
	  {"log.connection.close", false},
	  {"group.id", "foo"} //,{"debug", "all"}
	};
	cppkafka::MessageBuilder 	builder_series {params.topic.out_series};
	cppkafka::MessageBuilder 	builder_properties {params.topic.out_properties};
	cppkafka::MessageBuilder 	builder_logs {params.topic.out_logs};
	cppkafka::Producer       	producer(config);
	cppkafka::Consumer 			consumer(config);

	consumer.subscribe({params.topic.in});

	// on crée la machine de processors qui va regrouper les cascades par processor en fonction de leur source.
	tweetoscope::processing::Machine machine;

	while (true) {
		auto msg = consumer.poll(); //read
		if( msg && ! msg.get_error() ) {

			tweetoscope::tweet twt;

			//collects key (identifiant de la cascade):
			auto key = tweetoscope::cascade::idf(std::stoi(msg.get_key())); 
			
			//collects payload:
			auto istr = std::istringstream(std::string(msg.get_payload())); 
			istr >> twt;		

			// crée le nom du processeur en fonction de la source
			std::stringstream ss;
			ss << "Processor_" << std::to_string(twt.source);
			std::string processor_name = ss.str();

			// on stocke les valeurs (t, m) dans une paire
			std::pair<std::size_t, double> pair_t_m;
			pair_t_m.first = twt.time;
			pair_t_m.second = twt.magnitude;			

			if (twt.type == "tweet"){
				//on ajoute ou modifie le processor correspondant à la source twt.source .
				//on crée une cascade avec twt.msg
				
				std::ostringstream ostr;
				ostr 	<< std::endl 
						<< "Tweet of source: \""	<< twt.source 
						<< "\" and key: \""			<< key
						<< "\" was detected!"
						<< std::endl;
				std::cout << ostr.str(); 			

				machine += {processor_name, twt.source, key, twt.msg, pair_t_m, params.times.observation};
				
			}
			else{

				//identifie le processor responsable du retweet				
				auto it_p = machine.processors_.find(processor_name); 			
				if ( it_p != machine.processors_.end() ){

					// 1/ Tests for partial cascades:
					test_partial_cascade(it_p->second, builder_series, builder_logs, producer, pair_t_m, params);

					// 2/ Tests for cascade termination
					test_cascade_terminaison_and_update(it_p->first, it_p->second, builder_properties, builder_logs, producer, pair_t_m, params);
					
					// 3/ Processes retweet information:
					process_retweet(key, twt, it_p->second, pair_t_m);		
				}
			}		
		}
	}
  
  return 0;

}
