#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <tuple>
#include <stdexcept>

#include <memory>
#include <boost/heap/binomial_heap.hpp>
#include <iomanip>
#include <string>
#include <list>


namespace tweetoscope {
	namespace processing {

		class Cascade; // Now, we can talk about the class

		using cascade_ref  = std::shared_ptr<Cascade>;

		// This is the comparison functor for boost queues.
		struct cascade_ref_comparator {
			bool operator()(cascade_ref op1, cascade_ref op2) const; // Defined later.
		};

		// We define our queue type.
		using priority_queue = boost::heap::binomial_heap<cascade_ref,boost::heap::compare<cascade_ref_comparator>>;


		class Cascade{

			public:

			std::string cascade_name_; // corresponds to the twt message.
			int cascade_id_;
			std::size_t cascade_level_; // priority level, for the priority queue.
			std::list<std::pair<std::size_t, double>> time_serie_;

			priority_queue::handle_type location_; 
			// This is "where" the element is in the queue. This is needed to change the priority.
		
			Cascade(const std::string& cascade_name, int cascade_id, std::pair<std::size_t, double> pair_t_m) : cascade_name_(cascade_name), cascade_id_(cascade_id){
				this->cascade_level_= pair_t_m.first;
				this->time_serie_.push_back(pair_t_m);
			}

			bool operator<(const Cascade& other) const {return cascade_level_ > other.cascade_level_;}

			void operator=(int new_cascade_level) {cascade_level_ = new_cascade_level;}

			friend std::ostream& operator<<(std::ostream& os, const Cascade& c);

		};

		std::ostream& operator<<(std::ostream& os, const Cascade& c) {
			os 	<< "{\"cascade_name\": \"" 		<< c.cascade_name_ 
				<< "\", \"cascade_id\": \"" 	<< c.cascade_id_ 
				<< "\", \"cascade_level\": \"" 	<< c.cascade_level_
				<< "\", \"cascade_length\": \""	<< c.time_serie_.size()
				<< "\", \"cascade_time_serie\": \"{";
			for(auto& [ti,mi] : c.time_serie_) os << "(" << ti << ", " << mi << "), ";
			os 	<< "}\" } " << std::endl;
			return os;
		}

		// Now, we know how Cascade is made, we can write the comparison functor 
		// from < implemented in Cascade.
		bool cascade_ref_comparator::operator()(cascade_ref op1, cascade_ref op2) const {
			return *op1 < *op2;
		}

		// This is a convenient function for pointer allocation.
		cascade_ref cascade(const std::string& cascade_name, int cascade_id, std::pair<std::size_t, double> pair_t_m) {
			return std::make_shared<Cascade>(cascade_name, cascade_id, pair_t_m);
		}
				

		struct Processor {

			std::size_t source_id_;
			
			priority_queue cascades_queue_;

			//Map of the cascades associated to the processor (via weak pointers).			
			std::map< int, std::weak_ptr<Cascade> > locations_; 

			//Map of list of cascades per obersation window (via weak pointers).
			std::map< std::size_t, std::queue< std::weak_ptr<Cascade> > > windows_cascades_queue_;


			Processor(std::size_t source_id, int cascade_id, std::string cascade_name, std::pair<std::size_t, double> pair_t_m, std::vector<std::size_t> windows) : source_id_(source_id){
				auto ref = cascade(cascade_name,cascade_id,pair_t_m); //creates shared pointer.
				std::weak_ptr<Cascade> wp = ref; //first weak pointer.
				locations_.insert({cascade_id, wp});
				ref->location_=cascades_queue_.push(ref);

				std::weak_ptr<Cascade> wp_2 = ref; //second weak pointer.
				std::queue<std::weak_ptr<Cascade>> cascades_queue;
				cascades_queue.push(wp_2);
				for(const auto& window : windows) {
					windows_cascades_queue_.insert({window,cascades_queue});
				}	
			}

		};

		//operator<< défini pour la classe Processor basée sur les Queues
		std::ostream& operator<<(std::ostream& os, Processor p) {
			
			os << std::endl <<"Priority queue: "<< std::endl;
			while(!p.cascades_queue_.empty()) {
				auto ref = p.cascades_queue_.top();
				p.cascades_queue_.pop();
				os << *ref;
			}
			
			os << "Observation windows: ";
			for(const auto& window : p.windows_cascades_queue_) {
				os << std::endl << "	Window: \"" << window.first << "\" of size: \"" << window.second.size() << "\".";
			}
			
			return(os);
		}


		struct Machine {
			
			std::map<std::string, Processor> processors_;

			void operator+=(const std::tuple<std::string, std::size_t, int, std::string, std::pair<std::size_t, double>, std::vector<std::size_t> >& data) { // arg = {processor_name, source_id, cascade_id, cascade_name, pair_t_m, windows}
				// If the processor does not exist yet, we create it.
				// The arguments following the key of try_emplace fit the ones of a Processor constructor.
				auto [it, is_newly_created] = processors_.try_emplace(std::get<0>(data),std::get<1>(data),std::get<2>(data),std::get<3>(data),std::get<4>(data),std::get<5>(data));
				
				if(is_newly_created){			
					//std::cout << std::endl << it->first << " was created:" << it->second << std::endl << std::endl;				
				} 

				else{
					// create a cascade					
					cascade_ref ref = cascade(std::get<3>(data),std::get<2>(data),std::get<4>(data));
					// 1/ store it in the priority queue:
					ref->location_=it->second.cascades_queue_.push(ref);
					// 2/ keep track of it in the processor:					
					std::weak_ptr<Cascade> wp = ref;
					it->second.locations_.insert({std::get<2>(data), wp});
					// 3/ keep track of it in the time windows:
					for(const auto& window : std::get<5>(data)) {
						std::weak_ptr<Cascade> wp_w = ref; //inside the loop so that each time window as a different wp
						auto it_w = it->second.windows_cascades_queue_.find(window);
						if ( it_w != it->second.windows_cascades_queue_.end() ){
							it_w->second.push(wp_w);
						}
					}		

					//std::cout << std::endl << it->first << " was modified:" << it->second << std::endl << std::endl;
				}
			}  
			
			// This removes a processor
			void operator-=(const std::string& processor_name) {
				if(auto it = processors_.find(processor_name); it != processors_.end()){
					processors_.erase(it);
					std::cout << "Deletion: \"" << it->first << " was removed." <<std::endl;
				}
			}

		};    

		std::ostream& operator<<(std::ostream& os, const Machine& machine) {
			os << "Machine : " << std::endl;
			for(auto& name_processor : machine.processors_)
				os << "  " << name_processor.first << " : " << name_processor.second << std::endl;
			return os;
		}

	}
}
