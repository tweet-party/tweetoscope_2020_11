from kafka import KafkaConsumer, KafkaProducer
import json
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('--broker-list', type=str,
                        help="The broker list. It could be either \n"
                        " - a filepath : containing a comma separated list"
                        " of brokers\n"
                        " - a comma separated list of brokers, e.g. "
                        " localhost:9091,localhost:9092\n",
                        required=True)
    args = parser.parse_args()

    #The broker is printed to confirm which one has been selected
    print("\nBroker list is: " + args.broker_list + "\n ")

    #Kafka Consumer
    consumer_samples = KafkaConsumer('stats',                        # Topic name
            bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
            value_deserializer=lambda v: json.loads(v.decode('utf-8'))  # How to deserialize the value from a binary buffer
            #key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
    )

    #Stores the values of the 600/1200 window for comparison with the 1200/600 window
    w_600  = {}
    w_1200 = {}

    for msg in consumer_samples:
        cid = msg.value['cid']
        are = msg.value["ARE"]
        are_b = msg.value["ARE_boosted"]
        t_obs = msg.value['T_obs']

        #Message 600 is received ; if there is msg 1200 we display, otherwise we store and wait
        if (t_obs =="600"):
            if (cid in w_1200.keys()):
                try:
                    print(75*"*")
                    print("cid: "+str(cid)+" || Window 600  || ARE is "+str(are)+(10-len(str(are)))*" "+"|| ARE_boosted is "+str(are_b))
                    print("     "+len(str(cid))*" "+" || Window 1200 || ARE is "+str(w_1200[cid][0])+(10-len(str(w_1200[cid][0])))*" "+"|| ARE_boosted is "+str(w_1200[cid][1]))
                    del w_1200[cid]
                except Exception as e:
                    print("ERROR : Could not print the latest ARE", e)
            else:
                w_600[cid] = [are, are_b]

        #Message 1200 is received ; if there is msg 600 we display, otherwise we store and wait
        if (t_obs =="1200"):
            if (cid in w_600.keys()):
                try:
                    print(75*"*")
                    print("cid: "+str(cid)+" || Window 600  || ARE is "+str(w_600[cid][0])+(10-len(str(w_600[cid][0])))*" "+"|| ARE_boosted is "+str(w_600[cid][1]))
                    print("     "+len(str(cid))*" "+" || Window 1200 || ARE is "+str(are)+(10-len(str(are)))*" "+"|| ARE_boosted is "+str(are_b))
                    del w_600[cid]
                except Exception as e:
                    print("ERROR : Could not print the latest ARE", e)
            else:
                w_1200[cid] = [are, are_b]
