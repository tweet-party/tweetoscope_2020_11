from kafka import KafkaConsumer, KafkaProducer
import json
import argparse

def np_plus (prompt,n_tot,dsp_size,n_threshold):
    res =min(dsp_size-len(prompt),(n_tot-n_threshold)//(n_threshold//20)+1) #from 0 to dsp_size with steps of n_threshold//20, taking the prompt length into account
    return max(res,0) #to ensure it never crashes

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('--broker-list', type=str,
                        help="The broker list. It could be either \n"
                        " - a filepath : containing a comma separated list"
                        " of brokers\n"
                        " - a comma separated list of brokers, e.g. "
                        " localhost:9091,localhost:9092\n",
                        required=True)
    parser.add_argument('--threshold', type=str,
                        help="The popularity threshold. \n"
                        " - an int: e.g. 200 \n",
                        required=True)
    args = parser.parse_args()

    #The broker is printed to confirm which one has been selected
    print("\nBroker list is: " + args.broker_list)
    print("\nThreshold is: " + args.threshold + "\n ")

    #Kafka Consumer
    consumer_samples = KafkaConsumer('alerts',                        # Topic name
            bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
            value_deserializer=lambda v: json.loads(v.decode('utf-8'))  # How to deserialize the value from a binary buffer
            #key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
    )

    n_threshold = int(args.threshold) #Threshold for n_tot
    dsp_size = 100 #Display size

    #Dict for popular tweets of window 600 and window 1200
    #Because tweets not necessarily arrive in order, sometimes 1200 is before 600
    w_600  = {}
    w_1200 = {}

    for msg in consumer_samples:
        cid = msg.value['cid']
        t_obs = msg.value['T_obs']
        n_tot = msg.value['n_tot']

        if ((t_obs == "600")):
            if (cid in w_1200.keys()): #The 1200 msg was received before the 600, so we don't need to store it in the w_600
                if (n_tot >= n_threshold):
                    print("ALERT : tweet of cid "+str(cid)+" is predicted to be popular (n_tot = "+str(n_tot)+")")
                    print("Popularity: "+ np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    print(dsp_size*"-")
                del w_1200[cid]
            else: #The 1200 msg was not yet received, so we store the message in the w_600
                if (n_tot >= n_threshold):
                    print("ALERT : tweet of cid "+str(cid)+" is predicted to be popular (n_tot = "+str(n_tot)+")")
                    print("Popularity: "+ np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    print(dsp_size*"-")
                w_600[cid] = n_tot

        if (t_obs == "1200"):
            if (cid in w_600.keys()): #We received the 600 message before, so no need to save in the w_1200
                if (n_tot >= n_threshold): #The 1200 message is a popular one
                    if (w_600[cid]>= n_threshold) and (w_600[cid]<n_tot): # 600 was popular but 1200 is more popular
                        print("ALERT : tweet of cid "+str(cid)+" is going to be even more popular (from n_tot = "+str(w_600[cid])+" to n_tot = "+str(n_tot)+")")
                        print("Popularity: "+np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    elif (w_600[cid]>= n_threshold) and (w_600[cid]>n_tot): # 600 was popular but 1200 is less popular
                        print("ALERT : tweet of cid "+str(cid)+" is going to be less popular (from n_tot = "+str(w_600[cid])+" to n_tot = "+str(n_tot)+")")
                        print("Popularity: "+np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    else: # 600 was not popular but 1200 is
                        print("ALERT : tweet of cid "+str(cid)+" is going to be popular after all (from n_tot = "+str(w_600[cid])+" to n_tot = "+str(n_tot)+")")
                        print("Popularity: "+np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    print(dsp_size*"-")
                else: # The 1200 message is not a popular one
                    if (w_600[cid]>= n_threshold): # The 600 message was a popular one and the 1200 is not
                        print("ALERT : tweet of cid "+str(cid)+" is not going to be popular after all (from n_tot = "+str(w_600[cid])+" to n_tot = "+str(n_tot)+")")
                        print(dsp_size*"-")
                del w_600[cid]
            else: #We have not received the 600 message before, so we save in the w_1200
                if (n_tot >= n_threshold):
                    print("ALERT : tweet of cid "+str(cid)+" is predicted to be popular (n_tot = "+str(n_tot)+")")
                    print("Popularity: "+np_plus("popularity: ",n_tot,dsp_size,n_threshold)*"+")
                    print(dsp_size*"-")
                w_1200[cid] = n_tot
