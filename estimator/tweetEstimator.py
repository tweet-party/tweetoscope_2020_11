from kafka import KafkaConsumer, KafkaProducer
import json 
import sys
import ast 
import argparse 
import numpy as np

import logger

from hawkes_process import compute_MAP, prediction
#import store_cascades as store

if __name__ == '__main__':
   parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
   parser.add_argument('--broker-list', type=str,
                     help="The broker list. It could be either \n"
                     " - a filepath : containing a comma separated list"
                     " of brokers\n"
                     " - a comma separated list of brokers, e.g. "
                     " localhost:9091,localhost:9092\n",
                     required=True)
   args = parser.parse_args()

   print("\nBroker list is: " + args.broker_list + "\n ")

   #initialize logger
   logger = logger.get_logger('estimator-node', broker_list=args.broker_list, debug=True) 

   #Creating Kafka Nodes
   consumer = KafkaConsumer('cascade_series', bootstrap_servers=args.broker_list, group_id="estimators")
   producer = KafkaProducer(bootstrap_servers=args.broker_list)

   for message in consumer:
      m = message.value.decode('utf-8')
      msg = ast.literal_eval(m) # Msg est un dictionnaire 
      #store.write_cascade(np.array(msg['tweets']),int(msg['cid']),'./cascade_files')
      res = compute_MAP(np.array(msg['tweets']),msg['tweets'][-1][0])
      p, beta = res[1]
      n_tot, G1 = prediction((p, beta), np.array(msg['tweets']), msg['tweets'][-1][0])
      #print(f"p = {p:.5f} & beta = {beta:.5f}")
      mm = {'type': 'parameters',  
                     'cid': msg['cid'],      
                     'msg' : msg['msg'],  
                     'n_obs': len(np.array(msg['tweets'])),          
                     'n_supp' : int(n_tot),  
                     'params': [p,beta,G1] }
      producer.send('cascade_properties', key = b'%d' % msg['T_obs'], value = json.dumps(mm).encode('utf-8'))
      producer.flush()

      logger.debug("Cascade " + str(msg['cid']) + ": predictions calculated for time window " + str(msg['T_obs']) + ".")


