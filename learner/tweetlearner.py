from kafka import KafkaConsumer, KafkaProducer
import json 
import argparse 
import numpy as np
import pickle

import logger

from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('--broker-list', type=str,
                        help="The broker list. It could be either \n"
                        " - a filepath : containing a comma separated list"
                        " of brokers\n"
                        " - a comma separated list of brokers, e.g. "
                        " localhost:9091,localhost:9092\n",
                        required=True)
    args = parser.parse_args()

    print("\nBroker list is: " + args.broker_list + "\n ")

    #initialize logger
    logger = logger.get_logger('learner-node', broker_list=args.broker_list, debug=True) 

    #Kafka Consumers
    consumer_samples = KafkaConsumer('samples',                        # Topic name
            bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
            value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
            key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
    )

    #Kafka Producers
    producer_models = KafkaProducer(
            bootstrap_servers = args.broker_list,                     # List of brokers passed from the command line
            value_serializer=lambda v: pickle.dumps(v),                # How to serialize the value to a binary buffer
            key_serializer=str.encode                                 # How to serialize the key
    )

    period = 4 # regr is re.fit every 4 new inputs
    regr_dict={} # {'obs_window': ([X,y],regr,stack) }

    for msg in consumer_samples:

        ##############################
        ## Update of (X,y) and regr ##
        ##############################

        if msg.key in regr_dict:
            #print("Time window {}: new input (X,y) detected.".format(msg.key))
            X,y=regr_dict.get(msg.key)[0]
            regr=regr_dict.get(msg.key)[1]
            stack=regr_dict.get(msg.key)[2]
            X.append(msg.value['X'])
            y.append(msg.value['W'])
            if period <= stack:
                regr.fit(X,y)
                stack=0
                producer_models.send('models', key = msg.key, value = regr) # directly send regr
                producer_models.flush()
                logger.debug("Regressor {} sent out.".format(msg.key))
            stack+=1
            regr_dict[msg.key]=((X,y),regr,stack)
        
        ###########################
        ## Create (X,y) and regr ##
        ###########################
 
        else:
            #print("New time window {}.".format(msg.key))
            X=[msg.value['X']]
            y=[msg.value['W']]
            regr = RandomForestRegressor(n_estimators=20, max_depth=None, random_state=42)
            regr_dict[msg.key]=((X,y),regr,1)




