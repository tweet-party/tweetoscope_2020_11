# Tweetoscope Project: Predict popularity of Tweets


_tweetoscope_ allows to predict the popularity of tweets from Twitter. This is an academic project
from CentraleSupélec as part of the Data Science major curriculum.

# Features

* _tweetoscope_ listens to random tweets generated from _generator_ and assembles them into Cascades with
_collector_. Each Cascade is then modelled as a self-exciting Hawkes point process by the _estimator_ 
and its total length is predicted by the _predicor_ using random forest models trained by _learner_. Prediction results are displayed in _Dashboard_ along side with precision statistics in _monitor_.

* _tweetoscope_ is built as a series of Kubernetes micro-services which communicate using [Kafka](https://kafka.apache.org/intro) topics.

# It's simple to deploy!

To deploy _tweetoscope_ you only need the Kubernetes yaml deployment files accessible here:

* the [_zookeeper-and-kafka.yml_](https://gitlab.com/tweet-party/tweetoscope_2020_11/-/jobs/artifacts/master/raw/zookeeper-and-kafka.yml?job=zookeeper_kafka_deployment_job) yaml file for zookeeper and kafka nodes deployment.
* the [_tweetoscope-deploy.yml_](https://gitlab.com/tweet-party/tweetoscope_2020_11/-/jobs/artifacts/master/raw/tweetoscope-deploy.yml?job=tweetoscope_deployment_job) yaml file for the _tweetoscope_ nodes deployment.

You can download these deployment files directly on your server! Using the following code:

```Shell
wget https://gitlab.com/tweet-party/tweetoscope_2020_11/-/jobs/artifacts/master/raw/zookeeper-and-kafka.yml?job=zookeeper_kafka_deployment_job -O zookeeper-and-kafka.yml
wget https://gitlab.com/tweet-party/tweetoscope_2020_11/-/jobs/artifacts/master/raw/tweetoscope-deploy.yml?job=tweetoscope_deployment_job -O tweetoscope-deploy.yml
```
In the event that above links are broken, deployment files can be manually retreived from the project source files
under the _yaml-files_ directory.

# Alternatively you can access the Docker Images of each node:

Using the following Docker Image link template: registry.gitlab.com/tweet-party/tweetoscope_2020_11/_node_ 
and replacing _node_ with each Kubernetes node (_generator, collector, estimator, predictor, learner, 
dashboard and monitor_).

You can then run a container from the selected image and connect it to your local machine network:

```Shell
docker run --rm -it --network host registry.gitlab.com/tweet-party/tweetoscope_2020_11/generator
```

Don't forget to have Zookeeper and Kafka running as well to enable node communications:
```Shell
docker run --rm -it --network host zookeeper
docker run --rm -it --network host --env KAFKA_BROKER_ID=0 --env KAFKA_LISTENERS=PLAINTEXT://:9092 --env KAFKA_ZOOKEEPER_CONNECT=localhost:2181 --env KAFKA_CREATE_TOPICS="tweets:1:1,cascade_series:4:1,cascade_properties:2:1,samples:1:1,models:1:1,alerts:1:1,stats:1:1,logs:1:1" wurstmeister/kafka
```
Which creates Kafka topics _tweets, cascade-series, cascade-properties, samples, models, alerts, stats and logs_.

# Finally you can decide to run the project from its source code:

In order to compile _generator_ and _collector_ you need:

* [_cppkafka_](https://github.com/mfontanini/cppkafka) which realies on: [_librdkafka_](https://github.com/edenhill/librdkafka) _>= 0.9.4_ and _CMake >= 3.9.2_
* [_gaml_](https://github.com/HerveFrezza-Buet/gaml)
* A compiler with good C++11 support (e.g. gcc >= 4.8). This was tested successfully on _g++ 4.8.3_. 
* The boost library

And run:

```Shell
g++ -o tweet-generator generator/tweet-generator.cpp -O3 $(pkg-config --cflags --libs gaml) -lpthread -lcppkafka
g++ -o tweetcollector collector/tweetoscopeCollector.cpp -O3 $(pkg-config --cflags --libs gaml) -lpthread -lcppkafka
```
The remaining nodes are written in python, to run them use python3.

