from kafka import KafkaConsumer, KafkaProducer
import json 
import argparse 
import numpy as np
import pickle

import logger

def compute_n_tot(n_obs, w_obs, params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   return int( n_obs+w_obs*G1/(1.-n_star) )

def compute_X (params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   return [beta,n_star,G1]

def compute_W (n_true, n_obs, params, alpha=2.4, mu=10):
   p,beta,G1=params
   n_star=p*mu*(alpha-1)/(alpha-2)
   try:
      res=(n_true-n_obs)*(1.-n_star)/G1
   except ZeroDivisionError as err:
      print('Handling run-time error:', err)
      res=-1
   return res
   

if __name__ == '__main__':
   parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
   parser.add_argument('--broker-list', type=str,
                     help="The broker list. It could be either \n"
                     " - a filepath : containing a comma separated list"
                     " of brokers\n"
                     " - a comma separated list of brokers, e.g. "
                     " localhost:9091,localhost:9092\n",
                     required=True)
   parser.add_argument('--obs-window', type=str,
                  help="The observation window. e.g. \n"
                  " 600 \n",
                  required=True)
   args = parser.parse_args()

   print("\nBroker list is: " + args.broker_list + "\n ")

   #initialize logger
   logger = logger.get_logger('predictor/{}-node'.format(args.obs_window), broker_list=args.broker_list, debug=True) 

   #Kafka Consumers
   consumer_properties = KafkaConsumer('cascade_properties',          # Topic name
         bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
         value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
         key_deserializer= lambda v: v.decode(),                      # How to deserialize the key (if any)
         group_id="estimators-{}".format(args.obs_window)             # Group_id necessary for parallelisation of nodes
   )
   consumer_models = KafkaConsumer('models',                          # Topic name
         bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
         value_deserializer=lambda v: pickle.loads(v),                # How to deserialize the value from a binary buffer
         key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
   )

   #Kafka Producers
   producer_alerts = KafkaProducer(
         bootstrap_servers = args.broker_list,                     # List of brokers passed from the command line
         value_serializer=lambda v: json.dumps(v).encode('utf-8') # How to serialize the value to a binary buffer
         #key_serializer=str.encode                                 # How to serialize the key
   )
   producer_samples = KafkaProducer(
         bootstrap_servers = args.broker_list,                     # List of brokers passed from the command line
         value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
         key_serializer=str.encode                                 # How to serialize the key
   )
   
   msg_dict={} # {key1:(value1,n_tot)} dictionary to stores partial messages, and n_tot if available
   regr_test=False # at init, no regressor is available
   
   while True:
      
      ###########################
      ## Handles Models inputs ##
      ###########################
   
      records = consumer_models.poll(1000)
      if records is None: continue
      else:
         for topicPartition, consumerRecords in records.items():
            
            for msg in consumerRecords:
               if msg.key == args.obs_window:
                  regr = msg.value
                  regr_test=True
                  logger.debug("Random Forest Regressor loaded.")

      
      #######################################
      ## Handles Cascade_Properties inputs ##
      #######################################
      
      records = consumer_properties.poll(1000)
      if records is None: continue
      else:
         for topicPartition, consumerRecords in records.items():
            
            for msg in consumerRecords:
               if msg.key == args.obs_window:

                  #######################
                  ## Parameters inputs ##
                  #######################

                  if msg.value['type']=='parameters':
                     print("\nparameter msg of id {} detected.".format(msg.value["cid"]))
                     n_tot=0
                     
                     if regr_test:
                        w_rf=regr.predict([compute_X(msg.value['params'])])[0] # regr sends back [res]
                        n_tot=compute_n_tot(msg.value['n_obs'],w_rf,msg.value['params'])
                        msg_alerts = {'type': 'alert',  
                                    'cid': msg.value['cid'], 
                                    'msg' : msg.value['msg'],     
                                    'T_obs' : msg.key,  
                                    'n_tot': n_tot
                                    }
                        producer_alerts.send('alerts', key = None, value = msg_alerts)
                        producer_alerts.flush()
                        logger.debug("Cascade {}: alert sent.".format(msg.value['cid']))

                     msg_dict[msg.value["cid"]]=(msg.value,n_tot,msg.value['n_supp'])

                  #################
                  ## Size inputs ##
                  #################

                  if msg.value['type']=='size':
                     print("\nsize msg of id {} detected.".format(msg.value["cid"]))
                     
                     if msg.value["cid"] in msg_dict:
                        storage=msg_dict.pop(msg.value["cid"])
                        partial_value=storage[0]
                        X=compute_X(partial_value['params'])
                        W=compute_W(msg.value['n_tot'],partial_value['n_obs'],partial_value['params'])
                        if W != -1: # throws weird values of W
                           msg_samples = {'type': 'sample',  
                                          'cid': msg.value['cid'], 
                                          'X' : X,     
                                          'W' : W
                                          }
                           producer_samples.send('samples', key = args.obs_window, value = msg_samples)
                           producer_samples.flush()
                           logger.debug("Cascade {}: sample sent.".format(msg.value['cid']))
                        
                        if 0 < storage[1]: # check if regr was used for partial prediction
                           n_tot,n_supp=storage[1:]
                           ARE=np.abs(n_supp-msg.value['n_tot'])/msg.value['n_tot']
                           ARE_boosted=np.abs(n_tot-msg.value['n_tot'])/msg.value['n_tot']
                           msg_stats = {'type': 'stat',  
                                       'cid': msg.value['cid'],    
                                       'T_obs' : msg.key,
                                       'ARE' : "{:.4f}".format(ARE),
                                       'ARE_boosted': "{:.4f}".format(ARE_boosted)
                                       }
                           producer_alerts.send('stats', key = None, value = msg_stats)
                           producer_alerts.flush()
                           logger.debug("Cascade {}: stat sent.".format(msg.value['cid']))
                     
                     else:
                        print("no stored partial cascade matches this terminated cascade")
                  



